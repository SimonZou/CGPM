package com.cg.pm.entity;

import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;

/**
 * Created by Sinopec_Zou on 2017/3/2.
 */
@Entity
@Table(name = "pm_bug_info", schema = "pm")
public class PmBugInfo {
    private int id;
    private Integer projectId;
    private String title;
    private Date uptime;
    private Date colsetime;
    private String subName;
    private Integer state;
    private String checkName;
    private String operName;
    private String bugInfo;
    private String projectName;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "project_id", nullable = true)
    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "title", nullable = true, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "uptime", nullable = true)
    public Date getUptime() {
        return uptime;
    }

    public void setUptime(Date uptime) {
        this.uptime = uptime;
    }

    @Basic
    @Column(name = "colsetime", nullable = true)
    public Date getColsetime() {
        return colsetime;
    }

    public void setColsetime(Date colsetime) {
        this.colsetime = colsetime;
    }

    @Basic
    @Column(name = "sub_name", nullable = true, length = 20)
    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    @Basic
    @Column(name = "state", nullable = true)
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "check_name", nullable = true, length = 255)
    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    @Basic
    @Column(name = "oper_name", nullable = true, length = 255)
    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    @Basic
    @Column(name = "bug_info", nullable = true, length = 200)
    public String getBugInfo() {
        return bugInfo;
    }

    public void setBugInfo(String bugInfo) {
        this.bugInfo = bugInfo;
    }

    @Basic
    @Column(name = "project_name", nullable = true, length = 50)
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PmBugInfo pmBugInfo = (PmBugInfo) o;

        if (id != pmBugInfo.id) return false;
        if (projectId != null ? !projectId.equals(pmBugInfo.projectId) : pmBugInfo.projectId != null) return false;
        if (title != null ? !title.equals(pmBugInfo.title) : pmBugInfo.title != null) return false;
        if (uptime != null ? !uptime.equals(pmBugInfo.uptime) : pmBugInfo.uptime != null) return false;
        if (colsetime != null ? !colsetime.equals(pmBugInfo.colsetime) : pmBugInfo.colsetime != null) return false;
        if (subName != null ? !subName.equals(pmBugInfo.subName) : pmBugInfo.subName != null) return false;
        if (state != null ? !state.equals(pmBugInfo.state) : pmBugInfo.state != null) return false;
        if (checkName != null ? !checkName.equals(pmBugInfo.checkName) : pmBugInfo.checkName != null) return false;
        if (operName != null ? !operName.equals(pmBugInfo.operName) : pmBugInfo.operName != null) return false;
        if (bugInfo != null ? !bugInfo.equals(pmBugInfo.bugInfo) : pmBugInfo.bugInfo != null) return false;
        if (projectName != null ? !projectName.equals(pmBugInfo.projectName) : pmBugInfo.projectName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (projectId != null ? projectId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (uptime != null ? uptime.hashCode() : 0);
        result = 31 * result + (colsetime != null ? colsetime.hashCode() : 0);
        result = 31 * result + (subName != null ? subName.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (checkName != null ? checkName.hashCode() : 0);
        result = 31 * result + (operName != null ? operName.hashCode() : 0);
        result = 31 * result + (bugInfo != null ? bugInfo.hashCode() : 0);
        result = 31 * result + (projectName != null ? projectName.hashCode() : 0);
        return result;
    }
}
