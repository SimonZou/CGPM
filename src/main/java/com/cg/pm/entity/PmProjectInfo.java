package com.cg.pm.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Sinopec_Zou on 2017/2/23.
 */
@Entity
@Table(name = "pm_project_info", schema = "pm")
public class PmProjectInfo {
    private int id;
    private String name;
    private String nameE;
    private String mome;
    private int progress;
    private Date onlineTime;
    private Date startTime;
    private Date overTime;
    private String manger;



    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "name_e", nullable = false, length = 50)
    public String getNameE() {
        return nameE;
    }

    public void setNameE(String nameE) {
        this.nameE = nameE;
    }

    @Basic
    @Column(name = "mome", nullable = true, length = 200)
    public String getMome() {
        return mome;
    }

    public void setMome(String mome) {
        this.mome = mome;
    }

    @Basic
    @Column(name = "progress", nullable = false)
    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    @Basic
    @Column(name = "online_time", nullable = true)
    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    @Basic
    @Column(name = "start_time", nullable = true)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "over_time", nullable = true)
    public Date getOverTime() {
        return overTime;
    }

    public void setOverTime(Date overTime) {
        this.overTime = overTime;
    }

    @Basic
    @Column(name = "manger", nullable = true, length = 20)
    public String getManger() {
        return manger;
    }

    public void setManger(String manger) {
        this.manger = manger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PmProjectInfo that = (PmProjectInfo) o;

        if (id != that.id) return false;
        if (progress != that.progress) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (nameE != null ? !nameE.equals(that.nameE) : that.nameE != null) return false;
        if (mome != null ? !mome.equals(that.mome) : that.mome != null) return false;
        if (onlineTime != null ? !onlineTime.equals(that.onlineTime) : that.onlineTime != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (overTime != null ? !overTime.equals(that.overTime) : that.overTime != null) return false;
        if (manger != null ? !manger.equals(that.manger) : that.manger != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (nameE != null ? nameE.hashCode() : 0);
        result = 31 * result + (mome != null ? mome.hashCode() : 0);
        result = 31 * result + progress;
        result = 31 * result + (onlineTime != null ? onlineTime.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (overTime != null ? overTime.hashCode() : 0);
        result = 31 * result + (manger != null ? manger.hashCode() : 0);
        return result;
    }
}
