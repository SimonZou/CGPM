package com.cg.pm.dao;

import com.cg.pm.entity.PmProjectInfo;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Sinopec_Zou on 2017/2/24.
 */
public interface PmProjectsDao extends JpaRepository<PmProjectInfo,Integer>,JpaSpecificationExecutor<PmProjectInfo> {
}
