package com.cg.pm.dao;

import com.cg.pm.entity.PmBugInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Sinopec_Zou on 2017/2/23.
 */
public  interface PmBugInfoDao extends JpaRepository<PmBugInfo,Integer>,JpaSpecificationExecutor<PmBugInfo> {
    @Query("select  c from PmBugInfo  c where c.id=:d")
    PmBugInfo findbyId(@Param("d") Integer id);
}
