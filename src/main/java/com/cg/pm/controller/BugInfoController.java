package com.cg.pm.controller;

import com.alibaba.fastjson.JSON;
import com.cg.pm.dao.PmBugInfoDao;
import com.cg.pm.entity.PmBugInfo;
import com.jw.utils.pagehelper.jpa.Specifications;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SimonZou on 2017/3/1.
 */
@Controller
@RequestMapping("/bugInfo")
public class BugInfoController {
    private String modulePath = "bugInfo/";
    @Autowired
    PmBugInfoDao pmBugInfoDao;

    @GetMapping("/list")
    public String toList() {
        return this.modulePath + "list";
    }
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") Integer id, Model view){
        PmBugInfo vo = new PmBugInfo();
        if(id>0){
            vo = pmBugInfoDao.findbyId(id);
            view.addAttribute("operCode",9);
        }else{
            vo.setBugInfo("");
            vo.setTitle("");
            vo.setSubName("");
            vo.setProjectId(1);
            vo.setProjectName("金薇销售平台");
            view.addAttribute("operCode",3);
        }
        view.addAttribute("vo",vo);
        return this.modulePath+"edit";
    }

    @RequestMapping(value = "/list/doSearch/{page}/{size}/{search}",method = RequestMethod.GET)
    @ResponseBody
    public  String doSearch(@PathVariable("page") int page,
                            @PathVariable("size") int size,
                            @PathVariable("search") String searchAcc) {
        System.out.println(searchAcc);
        PmBugInfo pmBugInfo = JSON.parseObject(searchAcc, PmBugInfo.class);
        Specification<PmBugInfo> specification = Specifications.<PmBugInfo>builder()
                .like(StringUtils.isNotBlank(pmBugInfo.getSubName()) ,"name","%"+pmBugInfo.getSubName()+"%")
                .like(StringUtils.isNotBlank(pmBugInfo.getTitle()) ,"manger","%"+pmBugInfo.getTitle()+"%")
                .build();
        if (page<1) page = 1;//对翻页做限制
        Sort sort = new Sort("id");
        Page<PmBugInfo >  pageValue = pmBugInfoDao.findAll(specification,new PageRequest(page-1,size,sort));
        String value = JSON.toJSONString(pageValue);
        String string = JSON.toJSONString(pageValue.getContent());
        System.out.println(string);
        return value;
    }

    @RequestMapping(value ="/add" , method = RequestMethod.POST)
    @ResponseBody
    public String add(@RequestParam("dataObj") String data){
//        System.out.println(data);
        PmBugInfo  pmProjectInfo = JSON.parseObject(data, PmBugInfo.class);
        Map reslut = new HashMap<String,String>();
        try {
            pmProjectInfo.setProjectId(1);
            pmProjectInfo.setProjectName("金薇系统");
            pmProjectInfo.setUptime(new java.util.Date());
            pmBugInfoDao.save(pmProjectInfo);
            reslut.put("state",200);
        }catch (Exception e){
            reslut.put("state",411);
            reslut.put("msg",e.toString());
        }
        return JSON.toJSONString(reslut);
    }
    @RequestMapping(value ="/update" , method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestParam("dataObj") String data){
//        System.out.println(data);

        Map reslut = new HashMap<String,String>();
        try {
            PmBugInfo  formObj = JSON.parseObject(data, PmBugInfo.class);
            PmBugInfo toDao = pmBugInfoDao.findOne(formObj.getId());
            BeanUtils.copyProperties(formObj,toDao);
            toDao.setProjectId(1);
            toDao.setProjectName("金薇系统");
            toDao.setUptime(new java.util.Date());
            pmBugInfoDao.saveAndFlush(toDao);
            reslut.put("state",200);
        }catch (Exception e){
            reslut.put("state",411);
            reslut.put("msg",e.toString());
        }
        return JSON.toJSONString(reslut);
    }

    @RequestMapping(value ="/delete" , method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestParam String Ids){

        String[] IDS = Ids.split("/");
        Map reslut = new HashMap<String,String>();
        reslut.put("state","200");
        for (int i = 0;i<IDS.length;i++){
            try{
                pmBugInfoDao.delete(Integer.valueOf(IDS[i]));
            }catch (Exception e){
                reslut.put("state","411");
            }

        }

        return JSON.toJSONString(reslut);
    }
}
