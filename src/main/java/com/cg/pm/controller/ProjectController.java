package com.cg.pm.controller;

import com.alibaba.fastjson.JSON;
import com.cg.pm.dao.PmProjectsDao;
import com.cg.pm.entity.PmProjectInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import com.jw.utils.pagehelper.jpa.Specifications;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sinopec_Zou on 2017/2/24.
 */
@Controller
@RequestMapping("/project")
public class ProjectController {
    private String modulePath = "project/";
    @Autowired
    PmProjectsDao pmProjectsDao;

    @GetMapping("/list")
    public String toList() {
        return this.modulePath + "list";
    }

    @RequestMapping(value = "/list/doSearch/{page}/{size}/{search}",method = RequestMethod.GET)
    @ResponseBody
    public  String doSearch(@PathVariable("page") int page,
                            @PathVariable("size") int size,
                            @PathVariable("search") String searchAcc) {
        System.out.println(searchAcc);
        PmProjectInfo pmProjectinfo = JSON.parseObject(searchAcc, PmProjectInfo.class);
        Specification<PmProjectInfo> specification = Specifications.<PmProjectInfo>builder()
                .like(StringUtils.isNotBlank(pmProjectinfo.getName()) ,"name","%"+pmProjectinfo.getName()+"%")
                .like(StringUtils.isNotBlank(pmProjectinfo.getManger()) ,"manger","%"+pmProjectinfo.getManger()+"%")
                .build();
        if (page<1) page = 1;//对翻页做限制
        Sort sort = new Sort("id");
        return JSON.toJSONString(pmProjectsDao.findAll(specification,new PageRequest(page-1,size,sort)));
    }

    @RequestMapping(value ="/add" , method = RequestMethod.POST)
    @ResponseBody
    public String add(@RequestParam("dataObj") String data){
//        System.out.println(data);
        PmProjectInfo  pmProjectInfo = JSON.parseObject(data, PmProjectInfo.class);
        Map reslut = new HashMap<String,String>();
        try {
            pmProjectsDao.save(pmProjectInfo);
            reslut.put("state",200);
        }catch (Exception e){
            reslut.put("state",411);
            reslut.put("msg",e.toString());
        }
        return JSON.toJSONString(reslut);
    }
    @RequestMapping(value ="/update" , method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestParam("dataObj") String data){
//        System.out.println(data);

        Map reslut = new HashMap<String,String>();
        try {
            PmProjectInfo  pmProjectInfoFrom = JSON.parseObject(data, PmProjectInfo.class);
            PmProjectInfo fromDaoProjectInfo = pmProjectsDao.findOne(pmProjectInfoFrom.getId());
            BeanUtils.copyProperties(pmProjectInfoFrom,fromDaoProjectInfo);
            pmProjectsDao.saveAndFlush(fromDaoProjectInfo);
            reslut.put("state",200);
        }catch (Exception e){
            reslut.put("state",411);
            reslut.put("msg",e.toString());
        }
        return JSON.toJSONString(reslut);
    }
    @RequestMapping(value ="/delete" , method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestParam String Ids){
        System.out.println(Ids);
        String[] IDS = Ids.split("/");
        Map reslut = new HashMap<String,String>();
        reslut.put("state","200");
        for (int i = 0;i<IDS.length;i++){
            try{
                pmProjectsDao.delete(Integer.valueOf(IDS[i]));
            }catch (Exception e){
                reslut.put("state","411");
            }

        }

        return JSON.toJSONString(reslut);
    }

}
