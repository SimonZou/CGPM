package com.cg.pm.controller;

import com.alibaba.fastjson.JSON;
import com.cg.pm.OSSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by SimonZou on 2017/3/3.
 */
@RestController
@RequestMapping("/file")
public class FileController {
    public static final String ROOT = "static/upload-dir";

    @Value("${alyuncs.bucket.head}")
    private String filePath = "http://jwbucket.oss-cn-shanghai.aliyuncs.com/";
    private final ResourceLoader resourceLoader;

    @Autowired
    public FileController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }



    @CrossOrigin("*")
    @RequestMapping(value = "/uploadPayInfo",method = RequestMethod.POST)
    public String uploadByPayInfo(String usersID,String orderNo,MultipartFile file){
        Random random = new Random(100);
        String srcFile = file.getOriginalFilename();
        String ext  =  srcFile.substring(srcFile.indexOf("."),srcFile.length());
        String newFileName = usersID+"_"+orderNo+"_payInfo"+random+ext;
        System.out.println("fileOriginalFileName>>" +srcFile+"newFileName >>>"+filePath+"/"+newFileName);
        try {
            OSSUtil.uploadFileToOss(newFileName,file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return JSON.toJSONString(this.getImageReslutHead(1,"上传失败",""));
        }
        return JSON.toJSONString(this.getImageReslutHead(0,"上传成功",filePath+"/"+newFileName));
    }
    @CrossOrigin("*")
    @RequestMapping(value = "/base64Upload",method = RequestMethod.POST)
    public String uploadBy64File(String usersID,MultipartFile file){

        String srcFile = file.getOriginalFilename();
        String ext  =  srcFile.substring(srcFile.indexOf("."),srcFile.length());
        String newFileName = usersID+"_head"+ext;
        System.out.println("fileOriginalFileName>>" +srcFile+"newFileName >>>"+filePath+"/"+newFileName);
        try {
            OSSUtil.uploadFileToOss(newFileName,file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return JSON.toJSONString(this.getImageReslutHead(1,"上传失败",""));
        }
        return JSON.toJSONString(this.getImageReslutHead(0,"上传成功",filePath+"/"+newFileName));
    }

    @RequestMapping(value="/upload",method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file")MultipartFile file){
        String fileName;
        if(!file.isEmpty()){
            try {

                fileName = file.getOriginalFilename();
                OSSUtil.uploadFileToOss(fileName,file.getInputStream());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return JSON.toJSONString(this.getImageReslut(1,"上传失败",file.getOriginalFilename()));
            } catch (IOException e) {
                e.printStackTrace();
                return JSON.toJSONString(this.getImageReslut(1,"上传失败",file.getOriginalFilename()));
            }

            return JSON.toJSONString(this.getImageReslut(0,"上传成功",file.getOriginalFilename()));
        }else{
            return JSON.toJSONString(this.getImageReslut(1,"文件不存在",file.getOriginalFilename()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable String filename) {

        try {
            return ResponseEntity.ok(resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString()));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    Map getImageReslutHead(int code,String msg,String src){
        Map map  = new HashMap<String,Object>();
        Map dataMap = new HashMap<String,Object>();
        dataMap.put("src",src);
        map.put("code",code);
        map.put("msg",msg);
        map.put("data",dataMap);

        return map;
    }
    Map getImageReslut(int code,String msg,String src){
        Map map  = new HashMap<String,Object>();
        Map dataMap = new HashMap<String,Object>();
        dataMap.put("src",filePath+src);
        map.put("code",code);
        map.put("msg",msg);
        map.put("data",dataMap);

        return map;
    }
}
