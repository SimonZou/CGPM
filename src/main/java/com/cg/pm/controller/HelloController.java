package com.cg.pm.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.Map;


/**
 * Created by Sinopec_Zou on 2017/2/22.
 */
@Controller
public class HelloController {
    @Value("${application.message}")
    private String message = "hi,hello world......";

    @RequestMapping("/hello")
    public String web(Map<String,Object> model){
//        model.put("time",new Date());
//        model.put("message",this.message);
        System.err.println("ni hao ");
        model.put("hello",this.message);
        return "frist";//返回的内容就是templetes下面文件的名称
    }
    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }
}
