package com.cg.pm.controller;


import com.cg.pm.dao.PmBugInfoDao;
import com.cg.pm.entity.PmBugInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Sinopec_Zou on 2017/2/22.
 */
@RestController
@RequestMapping("/test")
public class BugInfoApiController {
    @Autowired
    private PmBugInfoDao bugInfoMapper;

    @RequestMapping("/BugInfoList/{id}")
    public PmBugInfo getBugInfo(@PathVariable("id") Long id){
        return bugInfoMapper.findOne(1);
    }
}
