package com.cg.pm.controller;

import com.cg.pm.utils.KdniaoTrackQueryAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Created by xiaobai on 2017/2/14.
 */
@Controller
public class KdniaoTrackQueryController {
    @Autowired
    KdniaoTrackQueryAPI kdniaoTrackQueryAPI;
    private String modulePath = "Kdniao/";

    @RequestMapping(value = "/KdniaoTrackQuery/{KDId}", method = RequestMethod.GET)
    public @ResponseBody String showBlogs(@PathVariable("KDId") String KDId) {
        String result = null;
        try {
            result = kdniaoTrackQueryAPI.getOrderTracesByJson("SF", KDId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "/Kdniao/{kdh}",method = RequestMethod.GET)
    public String toList(@PathVariable("kdh")String kdh, ModelMap modelMap) {
        String result = null;
        try {
            result = kdniaoTrackQueryAPI.getOrderTracesByJson("SF", kdh);
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("isShow",false);  //不显示输入框
        modelMap.addAttribute("result",result);
        return this.modulePath + "KdniaoTrackQuery";
    }
    @RequestMapping(value = "/Kdniao",method = RequestMethod.GET)
    public String kdniao(ModelMap modelMap) {
        modelMap.addAttribute("isShow",true);   //显示输入框
        return this.modulePath + "KdniaoTrackQuery";
    }
}
