/*
Navicat MySQL Data Transfer

Source Server         : PMDB
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : pm

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2017-02-27 19:42:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pm_bug_info
-- ----------------------------
DROP TABLE IF EXISTS `pm_bug_info`;
CREATE TABLE `pm_bug_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `bug_info` varchar(200) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `uptime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '提交时间',
  `colsetime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '关闭时间',
  `subName` varchar(20) DEFAULT NULL COMMENT 'BUG提交人员',
  `operName` varchar(20) DEFAULT NULL COMMENT 'BUG处理人员',
  `checkName` varchar(20) DEFAULT NULL COMMENT '验证人员',
  `state` int(2) DEFAULT NULL,
  `check_name` varchar(255) DEFAULT NULL,
  `oper_name` varchar(255) DEFAULT NULL,
  `sub_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PM_BUG_INFO_PM_PROJECT_INFO_1` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pm_bug_info
-- ----------------------------
INSERT INTO `pm_bug_info` VALUES ('1', null, '1', '1', '2017-02-22 18:18:04', '2017-02-22 18:18:09', '1', '1', '1', '1', null, null, null);

-- ----------------------------
-- Table structure for pm_members_info
-- ----------------------------
DROP TABLE IF EXISTS `pm_members_info`;
CREATE TABLE `pm_members_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `level` int(2) DEFAULT NULL,
  `position` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pm_members_info
-- ----------------------------
INSERT INTO `pm_members_info` VALUES ('1', '胡巍', '13600000000', '1', '项目经理');

-- ----------------------------
-- Table structure for pm_project_info
-- ----------------------------
DROP TABLE IF EXISTS `pm_project_info`;
CREATE TABLE `pm_project_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_e` varchar(50) NOT NULL,
  `mome` varchar(200) DEFAULT NULL COMMENT '项目信息',
  `progress` int(2) NOT NULL COMMENT '项目进度',
  `manger` varchar(20) DEFAULT NULL,
  `online_time` date DEFAULT NULL,
  `over_time` date DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pm_project_info
-- ----------------------------
INSERT INTO `pm_project_info` VALUES ('1', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '10', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('2', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '20', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('3', '销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '30', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('4', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '40', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('5', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '50', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('6', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '60', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('7', '金薇销售管理平台', 'jw-system', '金薇销售管理平台，综合一体化管理系统', '70', '胡巍', '2017-04-11', '2017-06-15', '2016-11-15');
INSERT INTO `pm_project_info` VALUES ('8', '测试', '', '测试', '0', '测试', '2017-02-27', null, null);
INSERT INTO `pm_project_info` VALUES ('9', '测试2', '', '', '0', '测试2', '2017-02-27', null, null);

-- ----------------------------
-- Table structure for pm_project_members
-- ----------------------------
DROP TABLE IF EXISTS `pm_project_members`;
CREATE TABLE `pm_project_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membersId` int(20) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL,
  `members_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PROJECT_ID` (`projectId`),
  KEY `FK_MEMBERS_ID` (`membersId`),
  CONSTRAINT `FK_MEMBERS_ID` FOREIGN KEY (`membersId`) REFERENCES `pm_members_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pm_project_members
-- ----------------------------
INSERT INTO `pm_project_members` VALUES ('1', '1', '1', '1', null, null, null);
