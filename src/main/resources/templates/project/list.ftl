<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>项目列表</title>
<#include "/commpent/head.ftl"/>
</head>
<body>
<div class="layui-layer-content" id="MainId">
<#--查询条件及操作台-->
    <fieldset class="layui-elem-field">
        <legend class="layui-field-title">项目列表</legend>
        <div class="layui-field-box" style="margin-top: -20px;">
            <div class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">项目名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input"
                                   value="{{search.name}}" v-model="search.name">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">项目经理</label>
                        <div class="layui-input-inline">
                            <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input"
                                   value="{{search.manger}}" v-model="search.manger">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-btn-group" style="margin-top: -10px;">
                <button class="layui-btn layui-btn-small layui-btn-warm" v-on:click="searchfun()"><i class="layui-icon">
                    &#xe615;</i>查询
                </button>
                <button class="layui-btn layui-btn-small" v-on:click="openWin()"><i class="layui-icon">&#xe654;</i>增加
                </button>
                <button class="layui-btn layui-btn-small layui-btn-normal" v-on:click="editFun()"><i class="layui-icon">&#xe642;</i>编辑
                </button>
                <button class="layui-btn layui-btn-small layui-btn-danger"><i class="layui-icon">&#xe643;</i> 删除
                </button>
            </div>
        </div>

    </fieldset>
<#--表单-->
    <div class="layui-field-box">
        <table class="layui-table" lay-even="">
            <colgroup>
                <col width="120">
                <col width="120">
                <col width="120">
                <col width="120">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <td>{{head.id}}</td>
                <td>{{head.name}}</td>
                <td>{{head.manger}}</td>
                <td>{{head.onlineTime}}</td>
                <td>{{head.progress}}</td>
                <td>{{head.mome}}</td>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!dateLength">
                <td colspan="6"><h1>没有数据</h1></td>
            </tr>
            <tr v-show="dateLength" v-for="item in TData ">
                <td>{{item.id}}
                    <input type="checkbox" v-on:click="checkedFun(item)" v-model="item.hasCheck">
                </td>
                <td>
                    <div class="layui-elip ">{{item.name}}</div>
                </td>
                <td>{{item.manger}}</td>
                <td>{{item.onlineTime| moment}}</td>
                <td>
                    {{item.progress}}%
                </td>
                <td>
                    <div class="layui-elip layui-inline">{{item.mome}}</div>
                </td>
            </tr>

            </tbody>
        </table>
        <div id="pagebarId"></div>
    </div>
<#--model-->
    <div v-show="edit" id="form">
        <div style="margin: 20px;">
        <#--操作按钮-->
            <div class="layui-btn-group" style="margin-top: -5px;margin-bottom:-8px;">
                <button class="layui-btn layui-btn-small layui-btn-normal" v-on:click="saveEidt()">保存</button>
                <button class="layui-btn layui-btn-small layui-btn-danger" v-on:click="closeEidt()">关闭</button>
            </div>
            <hr>
        <#-- 表单-->
            <form class="layui-form layui-form-pane" action="">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label layui-bg-green">项目名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="title" autocomplete="off" placeholder="请输入项目名称" class="layui-input"
                                   lay-verify="required" v-model="tmpObject.name" value="{{tmpObject.name}}">
                        </div>
                        <label class="layui-form-label layui-bg-green">英文名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="title" autocomplete="off" placeholder="请输入项目英文名称"
                                   class="layui-input" lay-verify="required" v-model="tmpObject.nameE"
                                   value="{{tmpObject.nameE}}">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label layui-bg-green">项目经理</label>
                        <div class="layui-input-inline">
                            <input type="text" name="title" autocomplete="off" placeholder="请输入项目经理" class="layui-input"
                                   lay-verify="required" v-model="tmpObject.manger" value="{{tmpObject.manger}}">
                        </div>
                        <label class="layui-form-label layui-bg-green">上线时间</label>
                        <div class="layui-input-inline">
                            <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-mm-dd"
                                   autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this,choose:function(datas) {
                                       mainApp.tmpObject.onlineTime =datas;
                                        console.log(mainApp.tmpObject.onlineTime);
                                    }})"
                                   value="{{tmpObject.onlineTime| moment}}"
                                   v-model="tmpObject.onlineTime">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label layui-bg-green">开始时间</label>
                        <div class="layui-input-inline">
                            <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-mm-dd"
                                   autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this,choose:function(datas) {
                                       mainApp.tmpObject.startTime =datas;
                                        console.log(mainApp.tmpObject.startTime);
                                    }})"
                                   value="{{tmpObject.startTime| moment}}"
                                   v-model="tmpObject.startTime">
                        </div>
                        <label class="layui-form-label layui-bg-green">结束时间</label>
                        <div class="layui-input-inline">
                            <input type="text" name="IdoverTime" id="date" lay-verify="date" placeholder="yyyy-mm-dd"
                                   autocomplete="off" class="layui-input" onclick="layui.laydate({elem: this,choose:function(datas) {
                                       mainApp.tmpObject.overTime =datas;
                                        console.log(mainApp.tmpObject.overTime);
                                    }})"
                                   value="{{tmpObject.overTime| moment}}"
                                   v-model="tmpObject.overTime">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">项目简介</label>
                    <div class="layui-input-block">
                        <textarea placeholder="请输入内容" class="layui-textarea" value="{{tmpObject.mome}}" v-model="tmpObject.mome"></textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="${base}/js/vue.js"></script>
<script src="${base}/js/moment.js"></script>
<script src="${base}/js/jquery-3.1.1.min.js"></script>
<script>
    //    定义翻页控件
    var laypage;

    layui.use(['element', 'layer', 'laypage', 'jquery', 'form', 'laydate'], function () {
        var layer = layui.layer, laydate = layui.laydate;
        var form = layui.form();
        var $ = layui.jquery;
        var element = layui.element;
        laypage = layui.laypage;
//        layer.msg('Hello World');
        renderPage();
    })
    //全局变量
    var page = 1;   //默认为第一页
    var size = 10;   //默认为每页5行
    var pages = 1;
    var indexOpen;//记录弹出层

    function getDeleteSet() {       //拼装需要删除的行
        var result = "";
        result = result + "[";
        console.log(result);
        for (var i = 0; i < demo.checkedAcc.length; i++) {
            result = result + "{id:" + demo.checkedAcc[i].id + "}";
            result = result + ",";
        }
        console.log(result);
        result = result.substring(0, result.length - 1);
        result = result + "]";
        console.log(result);
        return result.toString();
    }
    function renderPage() {
        console.log("设置翻页" + page + ">>" + pages)
        laypage({
            cont: 'pagebarId'
            , pages: pages
            , curr: page
            , jump: function (obj) {
//                console.log("翻页到第" + obj.curr + "页")
                var reload = obj.curr == page ? false : true;
                page = obj.curr;
                if (reload) {
                    mainApp.pageSearchfun();
                }

            }
        });
    }
    var mainApp = new Vue({
        el: "#MainId",
        data: {
            active:1,
            selecteIds:[],
            checkedAcc: new Array(),
            search: {name: '', manger: ''},
            edit: false,
            head: {
                id: '序号',
                name: '项目名称',
                nameE: '项目英文名称',
                manger: '项目经理',
                onlineTime: "项目上线时间",

                progress: "项目进度",
                mome: "简介"
            },
            tmpObject: {
                hasCheck:false,
                id: 0, name: '', nameE: '',
                manger: '', onlineTime: moment().format('YYYY-MM-DD'), progress: 0, mome: '',startTime:'',overTime:''
            },
            dateLength: false,
            TData: []
        },
        created: function () {
            this.initdata(true);
//            renderPage();
        },
        methods: {
            initdata: function (rePageBar) {
                var _self = this;
                $.get("${base}/project/list/doSearch/" + page + "/" + size + "/" + JSON.stringify(_self.search), function (data) {
                    var _data = eval("(" + data + ")");
                    pages = _data.totalPages;
                    page = _data.number + 1;
                    _data = _data.content;
//                    console.log(_data);
                    if (_data.length == 0) {
                        _self.dateLength = false;
                    } else {
                        _self.dateLength = true;
                    }
                    _self.selecteIds = [];
                    _self.TData = _data;
                    for(var i= 0;i<_self.TData.length;i++){
                        _self.TData[i].hasCheck = false;
                    }
                    if (rePageBar) {
                        renderPage();
                    }

                })
            },
            searchfun: function () {
//                每次条件查询都要重置翻页
                page = 1;
                pages = 1;
                this.initdata(true)
            },
            pageSearchfun: function () {
                this.initdata(false)
            },
            add: function () {
                console.log("click add");
                this.edit = true;
                this.active = 1;
                this.openWin()
            },
            openWin: function () {
                var winWidth = document.body.innerWidth - 10;
                var winHigth = document.body.innerHeight+10;
                indexOpen = layer.open({
                    type: 1,
                    title: '项目信息',
                    scrollbar:true,
                    closeBtn: 0,//不显示关闭按钮
                    skin: 'layui-layer-rim', //加上边框
                    area: [winWidth+'px',winHigth+'px'], //宽高
                    content: $('#form')
                });
            },
            newTmpObj: function () {
                this.tmpObject = {
                    hasCheck:false,
                    id: 0, name: '', nameE: '',
                    manger: '', onlineTime: moment().format('YYYY-MM-DD'), progress: 0, mome: '',startTime:'',overTime:''
                };
            },
            saveEidt: function () {
                var _self = this;
                console.log(_self.tmpObject);
                var actUrl =  '${base}/project/add';
                if(_self.active!=1){
                    actUrl = '${base}/project/update';
                }
                $.ajax({
                    type: 'POST',
                    url:actUrl,
                    data: {dataObj: JSON.stringify(this.tmpObject)},
                    success: function (data) {
                        var _data = eval("(" + data + ")");
                        if(_data.state===200||_data.state=='200'){
                            _self.newTmpObj();
                            layer.msg("新增成功");
                            _self.searchfun();
                            _self.closeEidt();
                        }else{
                            layer.msg("保存出错");
                        }
//                        this.closeEidt();
                    },
                    error: function (data) {
                        layer.msg("保存出错");
                    }
                });

            },
            closeEidt: function () {
                layer.close(indexOpen);
            },
            checkedFun:function (item) {
                item.hasCheck = !item.hasCheck;
                if(item.hasCheck){

                    this.selecteIds.push(item.id);
//                    console.log("add id"+item.id+"》》length"+this.selecteIds.length);

                }else{
//                    console.log("rm id"+item.id);
                    this.removeArray(item.id);
//                    console.log(this.selecteIds);
                }
            },
            editFun:function () {
                if(this.selecteIds.length>1||this.selecteIds.length==0){
                    layer.msg("请选择一个编辑对象!");
                    return;
                }
                for(var i = 0;i<this.TData.length;i++){
                    if(this.TData[i].id == this.selecteIds[0]){
                        this.tmpObject = this.TData[i];
                        this.formatItem(this.tmpObject);
                        break;
                    }

                }
                this.active = 2;
                this.edit = true;
                this.openWin();
            },
            formatItem:function (item) {
                if(item.overTime){
                    item.overTime = moment(item.overTime).format('YYYY-MM-DD')
                }
                if(item.onlineTime){
                    item.onlineTime = moment(item.onlineTime).format('YYYY-MM-DD')
                }
                if(item.startTime){
                    item.startTime = moment(item.startTime).format('YYYY-MM-DD')
                }

            },
            removeArray:function (rm) {
                var rmIndex = 0;
                for(var i = 0;i<this.selecteIds.length;i++){
                    if(this.selecteIds[i]==rm){
                        rmIndex = i;
                    }
                }
                if(rmIndex==this.selecteIds.length-1){
                    this.selecteIds.length-=1;
                    return;
                }
                var tmpArr = this.selecteIds;
                for(var j = rmIndex;j<tmpArr.length-1;j++){
                    this.selecteIds[j]=tmpArr[j+1];
                }
                this.selecteIds.length-=1;
            }

        }
    })
    Vue.filter('moment', function (value) {
//        console.log(moment(new Date(parseInt(value)).toLocaleDateString()).format('HH:mm:ss'));
        return moment(new Date(parseInt(value)).toLocaleDateString()).format('YYYY-MM-DD');
    });
</script>
</body>
</html>