<#assign  base = request.contextPath />
<!DOCTYPE html>
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/form_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 基本表单</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${base}/hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${base}/hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${base}/hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${base}/hplus/css/animate.min.css" rel="stylesheet">
    <link href="${base}/hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInLeft">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <#if isShow >
                    <form method="get" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">快递号</label>
                            <div class="col-sm-8">
                                <input id="Query" type="text" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <input type="button" value="查询" class="btn btn-primary" onclick="KdniaoTrackQuery();">
                            </div>
                        </div>
                    </form>
                    </#if>
                    <div id="QueryResult"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${base}/hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="${base}/hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script>
    <#if !isShow>
    $(document).ready(function(){
        var resultObj = ${result};
        var list = resultObj.Traces;
        $("#QueryResult").html(getTimelineHtml(list));
    });
    </#if>
    function KdniaoTrackQuery() {
        if ($("#Query").val().length <= 1) {
            alert("请输入快递号")
            return;
        }
        $.get("/KdniaoTrackQuery/" + $("#Query").val(), function (result) {
            var resultObj = JSON.parse(result);
            var list = resultObj.Traces;

            $("#QueryResult").html(getTimelineHtml(list));
        });
    }

    function getResultHtml(list) {
        var resultHtml = "";
        if (list.length >= 1) {
            resultHtml = resultHtml + '<ul class="list-group">';
            for (var i = 0; i < list.length; i++) {
                resultHtml = resultHtml + '<li class="list-group-item">';
                resultHtml = resultHtml + '<span class="badge">' + list[i].AcceptTime + '</span>' + list[i].AcceptStation;
                resultHtml = resultHtml + '</li>';
            }
            resultHtml = resultHtml + '</ul>';
        } else {
            resultHtml = '<ul class="list-group">' +
                    '<li class="list-group-item">未查到相关快递信息</li>' +
                    '</ul>'
        }
        return resultHtml;
    }

    function getTimelineHtml(list) {
        var resultHtml = "";
        if (list.length >= 1) {
            resultHtml = resultHtml + '<div id="vertical-timeline" class="vertical-container light-timeline">';
            for (var i = 0; i < list.length; i++) {
                if (list[i].AcceptStation.indexOf("派件人") >= 0){
                    resultHtml = resultHtml + '<div class="vertical-timeline-block"><div class="vertical-timeline-icon navy-bg"><i class="iconfont">&#xe60c;</i></div><div class="vertical-timeline-content">';
                }else if(list[i].AcceptStation.indexOf("已签收") >= 0){
                    resultHtml = resultHtml + '<div class="vertical-timeline-block"><div class="vertical-timeline-icon navy-bg"><i class="iconfont">&#xe6c0;</i></div><div class="vertical-timeline-content">';
                }else {
                    resultHtml = resultHtml + '<div class="vertical-timeline-block"><div class="vertical-timeline-icon navy-bg"><i class="iconfont">&#xe742;</i></div><div class="vertical-timeline-content">';
                }
                resultHtml = resultHtml+'<h2>' + list[i].AcceptTime + '</h2> <p>' + list[i].AcceptStation + '</p>';
                resultHtml = resultHtml + '</div></div>';
            }
            resultHtml = resultHtml + '</div>';
        } else {
            resultHtml = '<ul class="list-group">' +
                    '<li class="list-group-item">未查到相关快递信息</li>' +
                    '</ul>'
        }
        return resultHtml;
    }
</script>
<style type="text/css">
    @font-face {font-family: 'iconfont';
        src: url('${base}/hplus/fonts/iconfont.eot'); /* IE9*/
        src: url('${base}/hplus/fonts/iconfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
        url('${base}/hplus/fonts/iconfont.woff') format('woff'), /* chrome、firefox */
        url('${base}/hplus/fonts/iconfont.ttf') format('truetype'), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
        url('${base}/hplus/fonts/iconfont.svg#iconfont') format('svg'); /* iOS 4.1- */
    }
    .iconfont{
        font-family:"iconfont" !important;
        font-size:16px;font-style:normal;
        -webkit-font-smoothing: antialiased;
        -webkit-text-stroke-width: 0.2px;
        -moz-osx-font-smoothing: grayscale;}
</style>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/form_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
</html>
