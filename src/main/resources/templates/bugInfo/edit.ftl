<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>项目列表</title>
<#include "/commpent/head.ftl"/>
</head>
<body>
<div style="margin: 20px;" id="formId">
<#--操作按钮-->
    <div class="layui-btn-group" style="margin-top: -5px;margin-bottom:-8px;">
        <button class="layui-btn layui-btn-small layui-btn-normal" v-on:click="saveEidt()">保存</button>
        <button class="layui-btn layui-btn-small layui-btn-danger" v-on:click="testFun()">关闭</button>
    </div>
    <hr>
<#-- 表单-->
    <form class="layui-form layui-form-pane" action="">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-block">
                <input type="text" name="title" autocomplete="off" placeholder="请输入标题" class="layui-input" value="{{entity.title}}" v-model="entity.title">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">项目名称</label>
                <div class="layui-input-block">
                    <input type="text" name="projectName"  placeholder="" autocomplete="off" class="layui-input" value="{{entity.projectName}}" disabled>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">提交人</label>
                <div class="layui-input-inline">
                    <input type="text" name="subName" autocomplete="off" placeholder="请输入提交人" class="layui-input" value="{{entity.subName}}" v-model="entity.subName">
                </div>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">文本域</label>
            <div class="layui-input-block">
                <textarea class="layui-textarea layui-hide" placeholder="请输入BUG信息" name="file" lay-verify="content" id="LAY_editor" >{{entity.bugInfo}}</textarea>
            </div>
        </div>
    </form>
</div>

<#--脚本吧-->
<script src="${base}/js/vue.js"></script>
<script src="${base}/js/moment.js"></script>
<script src="${base}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    //创建一个编辑器
    var editIndex =0;
    var layedit;
    layui.use(['form','layedit'], function () {

        var form = layui.form();
        layedit = layui.layedit;
        layedit.set({
            uploadImage: {
                url: '${base}/file/upload' //接口url
                ,type: 'POST' //默认post
            }
        });
        editIndex = layedit.build('LAY_editor');
//        layer.msg('Hello World');
//        renderPage();
    });
    function EditSetContext(){
        formApp.entity.bugInfo =  layedit.getContent(editIndex);
    };
    var formApp = new Vue({
        el:"#formId",
        data:{
            entity:{title:'${vo.title}',bugInfo:'${vo.bugInfo}',subName:'${vo.subName}',projectName:'${vo.projectName}',id:${vo.id},state:1},
            operCode:${operCode}
        },
        methods:{
            testFun:function () {
//                parent.mainApp.helloWorld("wo cong xia mail");
                parent.mainApp.closeEidt();
            },
            saveEidt:function () {
                var actUrl =  '${base}/bugInfo/add';
                if(this.operCode!=3){
                    actUrl = '${base}/bugInfo/update';
                }
                EditSetContext();
                $.ajax({
                    type: 'POST',
                    url:actUrl,
                    data: {dataObj: JSON.stringify(this.entity)},
                    success: function (data) {
                        var _data = eval("(" + data + ")");
                        if(_data.state===200||_data.state=='200'){

                            layer.msg("新增成功");
                            parent.mainApp.searchfun();
                            parent.mainApp.closeEidt();
                        }else{
                            layer.msg("保存出错");
                        }
//                        this.closeEidt();
                    },
                    error: function (data) {
                        layer.msg("保存出错");
                    }
                });
            }
        }
    });

</script>
</body>