<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>项目列表</title>
<#include "/commpent/head.ftl"/>
</head>
<body>
<div class="layui-layer-content" id="MainId">
<#--查询条件及操作台-->
    <fieldset class="layui-elem-field">
        <legend class="layui-field-title">问题登记表</legend>
        <div class="layui-field-box" style="margin-top: -20px;">
            <div class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">登记人</label>
                        <div class="layui-input-inline">
                            <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input"
                                   value="{{search.subName}}" v-model="search.subName">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-inline">
                            <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input"
                                   value="{{search.title}}" v-model="search.title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-btn-group" style="margin-top: -10px;">
                <button class="layui-btn layui-btn-small layui-btn-warm" v-on:click="searchfun()"><i class="layui-icon">
                    &#xe615;</i>查询
                </button>
                <button class="layui-btn layui-btn-small" v-on:click="add()"><i class="layui-icon">&#xe654;</i>增加
                </button>
                <button class="layui-btn layui-btn-small layui-btn-normal" v-on:click="editFun()"><i class="layui-icon">&#xe642;</i>编辑
                </button>
                <button class="layui-btn layui-btn-small layui-btn-danger"><i class="layui-icon">&#xe643;</i> 删除
                </button>
            </div>
        </div>

    </fieldset>
<#--表单-->
    <div class="layui-field-box">
        <table class="layui-table" lay-even="">
            <colgroup>
                <col width="120">
                <col width="120">
                <col width="120">
                <col width="120">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <td>{{head.id}}</td>
                <td>{{head.projectName}}</td>
                <td>{{head.title}}</td>
                <td>{{head.subName}}</td>
                <td>{{head.uptime}}</td>
                <td>{{head.state}}</td>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!dateLength">
                <td colspan="6"><h1>没有数据</h1></td>
            </tr>
            <tr v-show="dateLength" v-for="item in tableData ">
                <td>
                    <input type="checkbox" v-on:click="checkedFun(item)" v-model="item.hasCheck">
                </td>
                <td>
                    <div class="layui-elip ">{{item.projectName}}</div>
                </td>
                <td>{{item.title}}</td>
                <td>
                    {{item.subName}}%
                </td>
                <td>{{item.uptime|momentFilter}}</td>

                <td>
                    <div class="layui-elip layui-inline">{{item.state|stateFilter}}</div>
                </td>
            </tr>

            </tbody>
        </table>
        <div id="pagebarId"></div>
    </div>
</div>
<script src="${base}/js/vue.js"></script>
<script src="${base}/js/moment.js"></script>
<script src="${base}/js/jquery-3.1.1.min.js"></script>

<script>
    //    定义翻页控件,打开第几个层
    var laypage;
    var openIndex;
    var pages = 1;
    var page =1;
    var size = 5;
    layui.use(['element', 'layer', 'laypage', 'jquery', 'form', 'laydate'], function () {
        var layer = layui.layer, laydate = layui.laydate;
        var form = layui.form();
        var $ = layui.jquery;
        var element = layui.element;
        laypage = layui.laypage;
            console.log("init layui")
//        layer.msg('Hello World');
//        renderPage();
    });
    function renderPage() {
        //刷新翻页控件
        console.log("设置翻页" + page + ">>" + pages)
        laypage({
            cont: 'pagebarId'
            , pages: pages
            , curr: page
            , jump: function (obj) {
//                console.log("翻页到第" + obj.curr + "页")
                var reload = obj.curr == page ? false : true;
                page = obj.curr;
                if (reload) {
                    mainApp.initdata(false);
                }

            }
        });
    }


    var mainApp = new Vue({
        el: "#MainId",
        data: {
            active: 64,//add or update,when active=0  the model is hidde
            selecteIds: [],//row is checked
            edit:false,
            search: {subName:'',title:''},//search form proptype
            head: {id:'序号',projectName:'项目名称',title:'标题',subName:'提交人',uptime:'提交时间',state:'状态'},
            dateLength: false,
//            pageInfo: {pages:1, page:1, size:10},
            tableData: []
        },
        created: function () {

            this.initdata(true);
        },//the function is active on page loeader ;
        methods: {
            helloWorld:function (msg) {
                layer.msg(msg);
            },
            getUrl: function (act) {
//                console.log(this.pageInfo)
                //URL整合管理
                var url = "";
//                var page = this.pageInfo.page;
//                var size = this.pageInfo.size;
                switch (act) {
                    case 64:
                    url = '${base}/bugInfo/list/doSearch/' + page + '/' + size+'/'+JSON.stringify(this.search);
                        break;
                    case  2:
                        url = '${base}/bugInfo/edit/0';
                        break;
                    case  8:
                        url = '${base}/bugInfo/edit/'+this.selecteIds[0];
                        break;
                }
                return url;
            },

            paserTrDate:function (row) {
                row.hasCheck = false;
                console.log(row)
                return row;
            },

            initdata: function (rePageBar) {
                this.active = 64;
                var _self = this;
                var searchURL = this.getUrl(this.active);
                $.get(searchURL,function (data) {
                    var _data = eval("(" + data + ")");
                    pages = _data.totalPages;
                    page = _data.number + 1;
                    _self.tableData = _data.content;
                    if (_self.tableData.length == 0) {
                        _self.dateLength = false;
                    } else {
                        _self.dateLength = true;
                    }
                    _self.selecteIds = [];
                     for(var i= 0;i<_self.tableData.length;i++){
//                        _self.tableData[i] = _self.paserTrDate(_self.tableData[i]);//格式化每行数据
                         _self.tableData[i].hasCheck = false;
                    }
                    if (rePageBar) {
                        renderPage();
                    }
                })

            },
            searchfun: function () {
//                每次条件查询都要重置翻页
                page = 1;
                pages = 1;
                this.selecteIds = [];
                this.initdata(true);
            },
            add: function () {
                console.log("click add");
                this.edit = true;
                this.active = 2;
                this.openWin()
            },
            openWin: function () {
                var winWidth = document.body.innerWidth - 10;
                var winHigth = document.body.innerHeight+10;
                indexOpen = layer.open({
                    type: 2,
                    title: '项目信息',
                    scrollbar:true,
                    closeBtn: 1,//不显示关闭按钮
                    skin: 'layui-layer-rim', //加上边框
                    area: ['800px','600px'], //宽高
                    content: this.getUrl(this.active)
                });
            },
            closeEidt: function () {
                layer.close(indexOpen);
            },
            checkedFun:function (item) {
                item.hasCheck = !item.hasCheck;
                if(item.hasCheck){

                    this.selecteIds.push(item.id);
//                    console.log("add id"+item.id+"》》length"+this.selecteIds.length);

                }else{
//                    console.log("rm id"+item.id);
                    this.removeArray(item.id);
//                    console.log(this.selecteIds);
                }
            },
            editFun:function () {
                if(this.selecteIds.length>1||this.selecteIds.length==0){
                    layer.msg("请选择一个编辑对象!");
                    return;
                }
//                for(var i = 0;i<this.tableData.length;i++){
//                    if(this.tableData[i].id == this.selecteIds[0]){
//                        this.tmpObject = this.tableData[i];
//                        this.formatItem(this.tmpObject);
//                        break;
//                    }
//
//                }
                this.active = 8;
                this.edit = true;
                this.openWin();
            },
            removeArray:function (rm) {
                var rmIndex = 0;
                for(var i = 0;i<this.selecteIds.length;i++){
                    if(this.selecteIds[i]==rm){
                        rmIndex = i;
                    }
                }
                if(rmIndex==this.selecteIds.length-1){
                    this.selecteIds.length-=1;
                    return;
                }
                var tmpArr = this.selecteIds;
                for(var j = rmIndex;j<tmpArr.length-1;j++){
                    this.selecteIds[j]=tmpArr[j+1];
                }
                this.selecteIds.length-=1;
            }
        }
    });
    Vue.filter('stateFilter',function (value) {
        var val = "";
        switch (value){
            case 2:
                val='修改完成';
                break;
            default:
                val='未修改';
        }
        return val;
    });
    Vue.filter('momentFilter',function (value) {

        return moment(value).format('YYYY-MM-DD');
    });
</script>
</body>